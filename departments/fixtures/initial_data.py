from common.initial import InitialModel, register
from departments.models import Department, Employee

department_parents = [
    (None,), (None,), (1,), (1,), (1,), (1,), (2,), (2,), (2,), (2,),
    (None,), (None,), (3,), (3,), (3,), (3,), (13,), (13,), (13,), (13,),
    (20,), (20,), (20,), (21,), (21,),
]


@register
class InitialDepartment(InitialModel):
    model = Department

    @property
    def initial(self):
        for i, item in enumerate(department_parents, start=1):
            yield {'name': 'Отдел %s' % i, 'parent_id': item[0]}


@register
class InitialEmployee(InitialModel):
    model = Employee

    @property
    def initial(self):
        import random
        import datetime
        for i in range(1, 50000):
            yield {
                'f_name': 'ФИО %s' % i,
                'position': 'Должность %s' % i,
                'date': datetime.date.today,
                'salary': 1000,
                'department_id': random.randint(1, 25)
            }
