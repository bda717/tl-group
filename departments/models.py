from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey


class AbcCreated(models.Model):
    created_at = models.DateTimeField(_('Дата создания'), auto_now_add=True)
    updated_at = models.DateTimeField(_('Дата обновления'), auto_now=True)

    class Meta:
        abstract = True


class Department(MPTTModel, AbcCreated):
    name = models.CharField(_('Название'), max_length=255)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = _('Отдел')
        verbose_name_plural = _('Отделы')

    def __str__(self):
        return self.name


class Employee(AbcCreated):
    f_name = models.CharField(_('ФИО'), max_length=255)
    position = models.CharField(_('Должность'), max_length=255)
    date = models.DateField(_('Дата приема на работу'), max_length=255)
    salary = models.DecimalField(_('Заработная плата'), max_digits=7, decimal_places=2)

    department = models.ForeignKey(Department, verbose_name=_('Подразделение'),
                                   related_name='employees', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Сотрудник')
        verbose_name_plural = _('Сотрудники')

    def __str__(self):
        return self.f_name
