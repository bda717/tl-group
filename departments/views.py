from django.http import JsonResponse
from django.views.generic import TemplateView

from departments.models import Department, Employee


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            data=Department.objects.all(),
        )
        return context


def employees_json(request, department_id):
    employees = Employee.objects.filter(department_id=department_id)
    return JsonResponse(list(employees.values()), safe=False)
