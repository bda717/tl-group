from django.contrib import admin
from departments.models import Department, Employee
from mptt.admin import MPTTModelAdmin


@admin.register(Department)
class DepartmentAdmin(MPTTModelAdmin):
    pass


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('f_name', 'department', 'position', 'date', 'salary')
