import functools
import inspect
import os
import sys
from abc import ABCMeta, abstractproperty
from importlib import import_module

from django.apps import apps
from django.core import serializers
from django.core.management.base import BaseCommand, CommandError


def get_module_classes(module_name):
    module = import_module(module_name)
    for _, klass in inspect.getmembers(module, inspect.isclass):
        if klass.__module__ == module_name:
            yield klass


def ask_yes_no(func):
    """Check yes """

    @functools.wraps(func)
    def wrapper(self, value):
        if value:
            func(self)
        else:
            if hasattr(self, 'message'):
                message = self.message
            else:
                message = 'Do you want to do it?'
            while True:
                query = input('%s %s' % (message, '[Y/n]'))
                if query == '' or not query.lower() in ['yes', 'y', 'no', 'n']:
                    print('Please answer with yes or no!', file=sys.stderr)
                else:
                    if query.lower()[0] == 'y':
                        func(self)
                    break

    return wrapper


def update_or_create(model, m2m_fields=None, defaults=None, **kwargs):
    try:
        obj, created = model.objects.update_or_create(defaults=defaults, **kwargs)
        if m2m_fields:
            for k, v in m2m_fields.items():
                getattr(obj, k).set(v)
    except Exception as e:
        raise CommandError('%r does not updated or created.\n'
                           'Error: %s' % (model.__name__, e))
    else:
        act = 'created' if created else 'updated'
        print('Successfully %s %r' % (act, obj))
        return obj


def create_fixture(model):
    try:
        app_label, model_label = model.__module__.split('.')
        app_config = apps.get_app_config(app_label)
        path = '%s/fixtures/' % app_config.path
        file = '%s%s.json' % (path, model._meta.model_name)

        if not os.path.exists(path):
            try:
                os.mkdir(path)
            except Exception as e:
                raise SystemExit(e)

        with open(file, 'w') as out:
            serializers.serialize('json', model.objects.all(), stream=out, indent=2)
    except Exception as e:
        print(e)


class InitialModel(metaclass=ABCMeta):
    help = 'Initial data for model'


    @abstractproperty
    def model(self):
        pass

    @abstractproperty
    def initial(self):
        """
        Initial is an iterative object of a dictionary (field, value) used to update or create the model.
        """
        pass

    def __init__(self, *args, **kwargs):
        self.handle()

    def update_or_create(self, init, pk=1):
        for defaults in init:
            m2m_fields = {}
            for field in self.model._meta.get_fields():
                if field.__class__.__name__ == 'ManyToManyField':
                    m2m_fields[field.name] = defaults.pop(field.name)

            update_or_create(self.model, m2m_fields=m2m_fields, defaults=defaults, pk=pk)
            pk += 1

    def handle(self, *args, **options):
        try:
            self.model._meta.model_name
        except AttributeError as e:
            raise NameError('Model is not defined')
        self.update_or_create(self.initial)


class InitialModelCommand(BaseCommand):
    help = 'Initial data for model'

    @property
    def model(self):
        raise NotImplementedError

    @property
    def init(self):
        """
        Initial is an iterative object of the dictionary (field, value) used to update the object.
        """
        raise NotImplementedError

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        # message for @ask_yes_no decorator
        self.message = 'Do you want to update or create multiple rows from %r model?' % self.model.__name__

    def add_arguments(self, parser):
        parser.add_argument(
            '-f',
            '--fixture',
            action='store_true',
            help='Create fixture of model'
        )
        parser.add_argument(
            '-y',
            '--yes',
            action='store_true',
            help='Automatic yes to prompts'
        )

    def update_or_create(self, init, pk=1):
        for defaults in init:
            update_or_create(self.model, defaults, pk=pk)
            pk += 1

    @ask_yes_no
    def load(self, *args):
        self.update_or_create(self.init)

    def handle(self, *args, **options):
        try:
            self.model._meta.model_name
        except AttributeError as e:
            raise NameError('Model is not defined')

        self.load(options['yes'])

        if options['fixture']:
            create_fixture(self.model)
