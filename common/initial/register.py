from importlib import import_module


class InitialRegister:
    initial_data = {}

    @classmethod
    def register(cls, initial_model):
        print(cls.initial_data)
        app_label = initial_model.model._meta.app_label
        if not cls.initial_data.get(app_label):
            cls.initial_data[app_label] = []

        cls.initial_data[app_label].append(initial_model)

    @classmethod
    def get_initial_data(cls, app_label_or_model):
        is_model = False if isinstance(app_label_or_model, str) else True
        if is_model:
            model = app_label_or_model
            app_label = model._meta.app_label
        else:
            app_label = app_label_or_model

        # Import initial data classes. One time module
        import_module('%s.fixtures.initial_data' % app_label)

        if is_model:
            for initial_class in cls.initial_data[app_label]:
                if initial_class.model == model:
                    return [initial_class]
            return
        return cls.initial_data[app_label]

    def get(self):
        print(self.initial_data)


def register(cls):
    """
    Decorator for InitialModel subclass. Add initial data to InitialRegister.initial_data

    @use
    class InitialMenu(InitialModel):
        model = Menu
        initial = [{'title': 'Главное меню', 'key': 'main'}]
    """
    InitialRegister.register(cls)
    return cls
