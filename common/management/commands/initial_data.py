from django.apps import apps
from django.core.management.base import BaseCommand

from common.initial.register import InitialRegister


class Command(BaseCommand):
    """
    Create file:
    app_name/fixtures/initial_data.py

    The sequence of loading classes corresponds to the above file
    Сlass example:

    @register
    class InitialMenu(InitialModel):
        model = Menu

        @property
        def initial(self):
            yield {'title': 'Главное меню', 'key': 'main'}

    Run command:
    initial_data <app_label> <app_label>.<model_name> --all --fixture --yes
    """
    help = 'Installs the initial data from app_name/fixtures/initial_models.py'

    def add_arguments(self, parser):
        parser.add_argument(
            'args', metavar='app_label', nargs='*',
            help='Specify the app label(s) to install data. '
                 'Can be <app_label> or <app_label>.<model_name>'
        )

    #     parser.add_argument(
    #         '-a',
    #         '--all',
    #         action='store_true',
    #         help='Remove all migrations'
    #     )
    #     parser.add_argument(
    #         '-y',
    #         '--yes',
    #         action='store_true',
    #         help='Automatic yes to prompts'
    #     )
    #     parser.add_argument(
    #         '-f',
    #         '--fixture',
    #         action='store_true',
    #         help='Create fixture of model'
    #     )

    def load(self, app_label_or_model):
        initial_data = InitialRegister.get_initial_data(app_label_or_model)
        if initial_data:
            self.stdout.write('Loading initial data for %r' % app_label_or_model)
            for klass in initial_data:
                klass()
        else:
            self.stderr.write('Initial data for %r not determined.' % app_label_or_model)

    def handle(self, *app_labels, **options):
        if app_labels:
            for app_label in app_labels:
                try:
                    apps.get_app_config(app_label)
                except LookupError as err:
                    if '.' in app_label:
                        try:
                            model = apps.get_model(app_label)
                        except LookupError as e:
                            self.stderr.write(str(e))
                        except Exception as e:
                            self.stderr.write(str('%r, %s' % (app_label, e)))
                        else:
                            self.load(model)
                    else:
                        self.stderr.write(str(err))
                else:
                    self.load(app_label)
        else:
            self.stderr.write('Models or apps are not defined.')
