$(document).ready(function () {
    $(".bi").click(function () {
        let iconCls = $(this).attr("class");
        if (iconCls === "bi bi-plus") {
            let id = $(this).closest('tr').data("id");
            $("[data-parent='" + id + "'").removeClass("d-none");
            $(this).addClass("bi-dash").removeClass("bi-plus");
        } else if (iconCls === 'bi bi-dash') {
            let id = $(this).closest('tr').data("id");
            while ($("[data-parent='" + id + "'").length) {
                $("[data-parent='" + id + "'").addClass("d-none");
                $("[data-parent='" + id + "'").find("i").addClass("bi-plus").removeClass("bi-dash");
                id = $("[data-parent='" + id + "'").data("id");
            }
            $(this).addClass("bi-plus").removeClass("bi-dash");
        }
    });
});


$(function () {
    $(document).on("click", '.department', function () {
        let link = $(this)
        let id = $(this).data("department");
        let done = $(this).data("done");
        let url = "/employees/" + id

        if (!done) {
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    let html = ''
                    $.each(data, function (i, item) {
                        html += "<tr>" +
                            "<td>" + item.f_name + "</td>" +
                            "<td>" + item.position + "</td>" +
                            "<td>" + item.date + "</td>" +
                            "<td>" + item.salary + "</td></tr>";
                    });
                    $("#table_" + id).html(html);
                    link.data("done", true);
                },
                error: function (data) {
                    console.log('AJAX ERROR' + data.error);
                }
            });
        }
    });
});