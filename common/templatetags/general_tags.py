from django import template
from django.utils.html import format_html
from django.utils.safestring import mark_safe
register = template.Library()


@register.simple_tag
def get_space(value):
    if value == 0:
        return ''
    return mark_safe('&nbsp'*value*4)
